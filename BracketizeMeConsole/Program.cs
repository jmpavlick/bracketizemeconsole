﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using WikiTools.BracketizeMe;

namespace BracketizeMeConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			if (args.Any())
			{
				foreach (string folder in args)
				{
					Console.WriteLine(folder);
					Console.WriteLine();

					DirectoryInfo directoryInfo = new DirectoryInfo(folder);
					List<FileInfo> filesToBracketize = directoryInfo.GetFiles("*.sql", SearchOption.AllDirectories).ToList();

					foreach (FileInfo file in filesToBracketize)
					{
						Console.WriteLine(file.FullName);
						string fileText;
						using (StreamReader sr = new StreamReader(file.FullName))
						{
							fileText = sr.ReadToEnd();
						}
						
						Bracketizer b = new Bracketizer(fileText);
						
						using (StreamWriter sw = new StreamWriter(file.FullName, false))
						{
							sw.Write(b.BracketizedText);
						}
					}
				}

				Console.WriteLine("Bracketizing complete!");
			}
			else
			{
				Console.WriteLine("Please pass in a directory or series of directories to bracketize.");
				Console.WriteLine("Press any key to continue...");
				Console.ReadLine();
			}
		}
	}
}
