# BracketizeMe

## Explanation
* BracketizeMe adds bracket characters - `[` and `]` - to text strings in a SQL script that are not registered keywords, functions, etc.
	* For instance, `SELECT x.ColumnOne FROM TableX AS x` becomes `SELECT [x].[ColumnOne] FROM [TableX] AS [x]`

## Installation
* Copy this folder somewhere convenient on your hard drive. I usually put it in my Home directory, in a folder called `bin`.
* Add the BracketizeMe folder to your `PATH`
	* Here's a quick tutorial on adding things to your `PATH`: [Link](http://geekswithblogs.net/renso/archive/2009/10/21/how-to-set-the-windows-path-in-windows-7.aspx)
	* This allows you to call `BracketizeMe.exe` from a command prompt without being in the same directory as the BracketizeMe executable

## Usage
* BracketizeMe is a command-line application that bracketizes all SQL files in a given directory. From a command prompt, usage looks like this:
	* `> BracketizeMe.exe [folder name]`
	* To run the bracketizer without opening a command prompt window, navigate to the directory that you want to bracketize, double-click on the address bar, and replace the directory path with
		* `BracketizeMe .`

## Notes
* BracketizeMe is recursive by default - that is to say, it looks down through every folder and subfolder for files with a `.sql` extension.
* BrackcetizeMe does **not** change files that do not have a `.sql` extension.
* Back up your work to source control before you run BracketizeMe, and check the diff with your own two eyes before you try to run anything in SQL Server Management Studio.
